#include "worker.h"


Worker::Worker() {
    std::cerr << "\033[33;1mWorker()\033[0m\n"; 
}


Worker::~Worker(){ 
    std::cerr << "\033[33;1m~Worker()\033[0m\n"; 
    isWorking = false;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
}



void Worker::releaseThreadPtr(){
    if(thr){
        std::cerr << "\t\033[32;1mclear for thr = " << thr << "\033[0m\n";
        delete thr;
        thr = nullptr; 
    }
}

void Worker::run() {
    if(!thr) {
        thr = new std::thread(&Worker::worker, this);
        std::cerr << "\t\033[32;1mrun thr = " << thr << "\033[0m\n";
        thr->detach();
    }
}


void Worker::worker() {
    try{
        int i = 0;
         while ( i<99999999 && isWorking ) {
            std::unique_lock<std::mutex> lock(locker);
            data = rand();
     	    ++i;
	    if (i==999999) throw 123;
        }
    } catch(int e) {
     std::cerr << "\t\t\033[31;1mignore trow\033[0m" << e << std::endl;
    }
    releaseThreadPtr();
}


int Worker::getData() {
    std::unique_lock<std::mutex>  lock(locker);
    return data;
}
