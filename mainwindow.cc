#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent): QWidget(parent){
    initUI();
    connect(push, SIGNAL(clicked()), this, SLOT(Work()));
    tmr.setInterval(10); 
    connect(&tmr, SIGNAL(timeout()), this, SLOT(updateLabel()));
    tmr.start();
}


MainWindow::~MainWindow(){
}


void MainWindow::initUI(){
    vLayout = new QVBoxLayout(this);
    info = new QLabel("^__^");
    push = new QPushButton("push");
    vLayout->addWidget(info);
    vLayout->addWidget(push);
}


void MainWindow::Work(){
	worker.run();
}


void MainWindow::updateLabel(){
    info->setText(QString::number(worker.getData()));
}
