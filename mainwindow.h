#ifndef qwertyu
#define qwertyu

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTimer>
#include <QString>

#include "worker.h"


class MainWindow: public QWidget 
{
    Q_OBJECT

    public:
        MainWindow(QWidget *parent = nullptr);
         ~MainWindow();
        void initUI();

    public slots:
        void Work();
        void updateLabel();

    private:
        QLabel *info;
        QPushButton *push;
	QVBoxLayout *vLayout;
        QTimer tmr;
	Worker worker;
};

#endif
