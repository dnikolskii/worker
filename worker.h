#ifndef trewq
#define trewq

#include <string>
#include <iostream>
#include <thread>
#include <mutex>
#include <cstdlib>
#include <chrono>


class Worker {
public:
    Worker();
    ~Worker();
    void run();
    int getData();

private:
    int data {333};
    std::mutex locker;
    std::thread *thr {nullptr};
    void worker();
    bool isWorking {true};
    void releaseThreadPtr();
};

#endif
